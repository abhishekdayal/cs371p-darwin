// --------------
// RunDarwin.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Darwin.h"

// ------------
// darwin_solve
// ------------

// ----
// main
// ----

int main () {
    using namespace std;
    darwin_solve(cin, cout);
    return 0;
}