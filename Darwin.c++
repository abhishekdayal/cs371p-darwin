#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

#include <cassert>
#include <utility>
#include <list>
#include <stdlib.h>

using namespace std;


class Species {
public:
    enum instruction {
        //action
        hop,
        left,
        right,
        infect,
        //control
        if_empty,
        if_wall,
        if_random,
        if_enemy,
        go
    };

private:
    char _name;
    vector<pair<Species::instruction, int>> _program;

    static bool is_action(Species::instruction i) {
        if (i == Species::hop || i == Species::left || i == Species::right || i == Species::infect)
            return true;
        return false;
    }

public:
    Species(char n): _name(n), _program(0) {};

    friend ostream& operator << (ostream& w, const Species& s) {
        w << s._name;
        return w;
    }

    bool operator == (const Species& rhs) {
        return this->_name == rhs._name;
    }

    bool operator != (const Species& rhs) {
        return this->_name != rhs._name;
    }

    bool is_species(const char& lhs) {
        return lhs == this->_name;
    }

    void add_instruction(pair<Species::instruction, int> i) {
        _program.push_back(i);
    }

    pair<Species::instruction, int> next_action(int& pc, const bool& enemy, const bool& empty, const bool& wall) {
        pair<Species::instruction, int> curr = _program[pc];
        while(!is_action(curr.first)) {
            Species::instruction i = curr.first;
            int n = curr.second;
            if(i == Species::go || (i == Species::if_enemy && enemy)
                    || (i == Species::if_wall && wall) || (i == Species::if_empty && empty)) {
                pc = n;
            } else if(i == Species::if_random) {
                int r = rand();
                if(r % 2 == 1)
                    pc = n;
                else
                    ++pc;
            } else {// didn't meet any of the conditions, just go to the next instruction
                ++pc;
            }
            curr = _program[pc];
        }
        pc = curr.second;
        return curr;
    }
};

class Creature {
    enum direction {
        n,
        e,
        s,
        w
    };

private:
    Species* _s;
    int _pc;
    Creature::direction _d;
    int _turns;

public:
    Creature(Species* s, Creature::direction d):
    _s(s),
    _d(d),
    _pc(0),
    _turns(0) {}

    friend ostream& operator << (ostream& w, Creature& c) {
        w << *c._s;
        return w;
    }

    friend direction char_dir(const char& c);

    Species::instruction execute_turn(Creature* next, bool wall) {
        ++_turns;
        bool enemy = false;
        bool empty = false;
        if(!wall) {
            if (next == nullptr)
                empty = true;
            else if(this->_s != next->_s) {
                enemy = true;
            }
        }
        pair<Species::instruction, int> ret = _s->next_action(_pc, enemy, empty, wall);
        return ret.first;
    }

    pair<int, int> next(int& curr_row, int& curr_col) {
        if (_d == Creature::n)
            return make_pair(curr_row - 1, curr_col);
        if (_d == Creature::e)
            return make_pair(curr_row, curr_col + 1);
        if (_d == Creature::s)
            return make_pair(curr_row + 1, curr_col);
        return make_pair(curr_row, curr_col - 1);

    }

    bool should_move(int& darwin_turns) {
        return _turns < darwin_turns;
    }

    void turn_left() {
        _d = static_cast<direction>((_d + 3) % 4);
    }

    void turn_right() {
        _d = static_cast<direction>((_d + 1) % 4);
    }

    void infect(Creature* other) {
        if(other != nullptr) {
            other->_s = this->_s;
            other->_pc = 0;
        }
    }
};

Creature::direction char_dir(const char& c) {
    if (c=='n')
        return Creature::n;
    if (c=='e')
        return Creature::e;
    if (c=='s')
        return Creature::s;
    return Creature::w;
}

class Darwin_Board {
private:
    vector<Creature> _creatures;
    vector<vector<Creature*>> _grid;
    int _turns;

    bool is_wall(pair<int, int> coord) {
        int row = _grid.size();
        int col = _grid[0].size();
        if(coord.first < 0 || coord.first >= row
                ||coord.second < 0 || coord.second >= col) {
            return true;
        }
        return false;
    }

public:
    Darwin_Board(int& rows, int& cols):
    _turns(0) {
        while(rows) {
            vector<Creature*> row(cols);
            _grid.push_back(row);
            --rows;
        }
    }

    void print_header(ostream& w) {
        w <<"*** Darwin "<<_grid.size()<<"x"<<_grid[0].size()<<" ***"<<endl;
    }

    void print_board(ostream& w) {
        w << "Turn = " << _turns << ".\n";

        w << "  ";
        int nc = _grid[0].size();
        for(int i = 0; i < nc; ++i)
            w << (i%10);
        w << endl;
        int nr = 0;
        for(vector<Creature*> row : _grid) {
            w << (nr++ % 10) << " ";
            for(Creature* cp : row) {
                if(cp == nullptr)
                    w << ".";
                else
                    w << *cp;
            }
            w << endl;
        }
    }

    void add_creature(Creature& c, int& row, int& col) {
        _creatures.push_back(c);
        this->_grid[row][col] = &c;
    }

    void add_creatures(list<Creature>& cl, list<pair<int, int>>& init_loc) {
        assert(cl.size() == init_loc.size());
        list<pair<int, int> >::iterator il_b = init_loc.begin();
        for(Creature& c : cl) {
            pair<int, int> loc = *il_b;
            _creatures.push_back(c);
            this->_grid[loc.first][loc.second] = &(c);
            ++il_b;
        }
    }

    void next_turn() {
        ++_turns;
        int num_rows = _grid.size();
        int num_cols = _grid[0].size();
        for(int i = 0; i < num_rows; ++i) {
            for(int j = 0; j < num_cols; ++j) {
                Creature* cp = _grid[i][j];
                // null check
                if (cp != nullptr) {
                    // if creature has already had a turn, do not give it another
                    if((*cp).should_move(_turns)) {

                        Species::instruction action;
                        pair<int, int> next_coord = (*cp).next(i, j);
                        // figure out whether the spot in front of the creature is a wall or not, and return
                        // appropriate action
                        if(is_wall(next_coord)) {
                            action = (*cp).execute_turn(nullptr, true);
                        } else {
                            Creature* next = _grid[next_coord.first][next_coord.second];
                            action = (*cp).execute_turn(next, false);
                        }

                        // perform action
                        if(action == Species::left) {
                            (*cp).turn_left();
                        } else if(action == Species::right) {
                            (*cp).turn_right();
                        } else if(action == Species::hop) {
                            if(!is_wall(next_coord)) {
                                Creature* next = _grid[next_coord.first][next_coord.second];
                                if(next == nullptr) {
                                    _grid[next_coord.first][next_coord.second] = cp;
                                    _grid[i][j] = nullptr;
                                }
                            }
                        } else if (action == Species::infect) {
                            if(!is_wall(next_coord)) {
                                Creature* next = _grid[next_coord.first][next_coord.second];
                                (*cp).infect(next);
                            }
                        }
                    }
                }
            }
        }
    }
};

int test_read(istream& r) {
    int tests;
    r >> tests;
    // move to next line
    string name;
    getline(r, name);
    assert(tests > 0);
    return tests;
}


Darwin_Board board_read(istream& r) {
    int row;
    int col;
    r >> row;
    r >> col;
    Darwin_Board ret = Darwin_Board(row, col);
    // move to next line
    string name;
    getline(r, name);
    return ret;
}

int num_creatures_read(istream& r) {
    int c;
    r >> c;
    // move to next line
    string name;
    getline(r, name);
    assert(c > 0);
    return c;
}

void creatures_read(istream& r, vector<Species>& s_v, list<Creature>& to_add, list<pair<int, int>>& locations) {
    char species;
    int row;
    int col;
    char dir;
    r >> species;
    r >> row;
    r >> col;
    r >> dir;
    string name;
    getline(r, name);

    for(Species& s : s_v) {
        if(s.is_species(species)) {
            Creature ret = Creature(&s, char_dir(dir));
            to_add.push_back(ret);
            break;
        }
    }
    locations.push_back(make_pair(row,col));
}

vector<Species> species_init() {
    vector<Species> ret;

    // initialize action instructions with n of next instr
    Species f = Species('f');
    f.add_instruction(make_pair(Species::left, 1));
    f.add_instruction(make_pair(Species::go, 0));

    Species r = Species('r');
    r.add_instruction(make_pair(Species::if_enemy, 9));
    r.add_instruction(make_pair(Species::if_empty, 7));
    r.add_instruction(make_pair(Species::if_random, 5));
    r.add_instruction(make_pair(Species::left, 4));
    r.add_instruction(make_pair(Species::go, 0));
    r.add_instruction(make_pair(Species::right, 6));
    r.add_instruction(make_pair(Species::go, 0));
    r.add_instruction(make_pair(Species::hop, 8));
    r.add_instruction(make_pair(Species::go, 0));
    r.add_instruction(make_pair(Species::infect, 10));
    r.add_instruction(make_pair(Species::go, 0));

    Species t = Species('t');
    t.add_instruction(make_pair(Species::if_enemy, 3));
    t.add_instruction(make_pair(Species::left, 2));
    t.add_instruction(make_pair(Species::go, 0));
    t.add_instruction(make_pair(Species::infect, 4));
    t.add_instruction(make_pair(Species::go, 0));

    Species h = Species('h');
    h.add_instruction(make_pair(Species::hop, 1));
    h.add_instruction(make_pair(Species::go, 0));

    ret.push_back(f);
    ret.push_back(r);
    ret.push_back(t);
    ret.push_back(h);
    return ret;
}

void darwin_solve(istream& r, ostream& w) {
    vector<Species> species = species_init();
    int tests = test_read(r);
    while(tests) {
        Darwin_Board d = board_read(r);

        srand(0);
        d.print_header(w);

        // list of creatures to add to board
        list<Creature> to_add;
        // corresponding starting location of creature
        list<pair<int, int>> init_locations;

        int creatures = num_creatures_read(r);
        while(creatures) {
            creatures_read(r, species, to_add, init_locations);
            --creatures;
        }
        d.add_creatures(to_add, init_locations);

        int tot_moves;
        int freq;
        r >> tot_moves;
        r >> freq;
        // move to next line
        string name;
        getline(r, name);

        for(int i = 0; i <= tot_moves; ++i) {
            if( (i % freq) == 0 ) {
                d.print_board(w);
                if (i + freq <= tot_moves) {
                    w << "\n";
                }
            }
            d.next_turn();
        }

        --tests;
        if(tests)
            w << "\n";
    }
}

