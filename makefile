.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=                                  \
    Darwin.c++                           \
    Darwin.h                             \
    makefile                              \
    RunDarwin.c++                        \
    TestDarwin.c++							\
#    RunDarwin.in                         \
    RunDarwin.out                        \
    .gitignore                            \
    darwin-tests                         

# uncomment these four lines when you've created those files
# you must replace GitLabID with your GitLabID
#    darwin-tests/GitLabID-RunDarwin.in  \
#    darwin-tests/GitLabID-RunDarwin.out \
#    Darwin.log                           \
#    html                                  \

darwin-tests:
	git clone https://gitlab.com/gpdowning/cs371p-darwin-tests.git darwin-tests

html: Doxyfile Darwin.h
	doxygen Doxyfile

Darwin.log:
	git log > Darwin.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	doxygen -g

RunDarwin: Darwin.h Darwin.c++ RunDarwin.c++
	-cppcheck Darwin.c++
	-cppcheck RunDarwin.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Darwin.c++ RunDarwin.c++ -o RunDarwin

RunDarwin.c++x: RunDarwin
	./RunDarwin < RunDarwin.in > RunDarwin.tmp
	-diff RunDarwin.tmp RunDarwin.out

TestDarwin: Darwin.h Darwin.c++ TestDarwin.c++
	-cppcheck Darwin.c++
	-cppcheck TestDarwin.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra  Darwin.c++ TestDarwin.c++ -o TestDarwin -lgtest -lgtest_main -pthread

TestDarwin.c++x: TestDarwin
	valgrind ./TestDarwin
	gcov -b Darwin.c++ | grep -A 5 "File '.*Darwin.c++'"

all: RunDarwin TestDarwin

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunDarwin
	rm -f TestDarwin

config:
	git config -l

ctd:
	checktestdata TestDarwin.ctd RunDarwin.in

docker:
	docker run -it -v $(PWD):/usr/darwin -w /usrdarwin gpdowning/gcc

format:
	astyle Darwin.c++
	astyle Darwin.h
	astyle RunDarwin.c++
	astyle TestDarwin.c++

#init:
#	touch README
#	git init
#	git remote add origin git@gitlab.com:gpdowning/cs371p-collatz.git
#	git add README
#	git commit -m 'first commit'
#	git push -u origin master

#pull:
#	make clean
#	@echo
#	git pull
#	git status

#push:
#	make clean
#	@echo
#	git add .gitignore
#	git add .gitlab-ci.yml
#	git add Collatz.c++
#	git add Collatz.h
#	-git add Collatz.log
#	-git add html
#	git add makefile
#	git add RunCollatz.c++
#	git add RunCollatz.in
#	git add RunCollatz.out
#	git add Test.ctd
#	git add TestCollatz.c++
#	git commit -m "another commit"
#	git push
#	git status

run: RunDarwin.c++x TestDarwin.c++x

#scrub:
#	make clean
#	rm -f  Collatz.log
#	rm -f  Doxyfile
#	rm -rf collatz-tests
#	rm -rf html
#	rm -rf latex

#status:
#	make clean
#	@echo
#	git branch
#	git remote -v
#	git status

versions:
	which         astyle
	astyle        --version
	@echo
	dpkg -s       libboost-dev | grep 'Version'
	@echo
	ls -al        /usr/lib/*.a
	@echo
	which         checktestdata
	checktestdata --version
	@echo
	which         cmake
	cmake         --version
	@echo
	which         cppcheck
	cppcheck      --version
	@echo
	which         doxygen
	doxygen       --version
	@echo
	which         g++
	g++           --version
	@echo
	which         gcov
	gcov          --version
	@echo
	which         git
	git           --version
	@echo
	which         make
	make          --version
	@echo
	which         valgrind
	valgrind      --version
	@echo
	which         vim
	vim           --version
