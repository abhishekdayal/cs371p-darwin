#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

#include <cassert>
#include <utility>
#include <list>
#include <stdlib.h>

using namespace std;

/**
 * Class that represents a Species
 * Stores the program instructions for each species
 * Provides the ability to add instructions to a species without,
 * with no need to loop through all creatures that are of that species
*/
class Species {
public:
    enum instruction {
        //action
        hop,
        left,
        right,
        infect,
        //control
        if_empty,
        if_wall,
        if_random,
        if_enemy,
        go
    };

private:
    char _name;
    vector<pair<Species::instruction, int>> _program;

    static bool is_action(Species::instruction i);

public:
    Species(char n);

    friend ostream& operator << (ostream& w, const Species& s);

    bool operator == (const Species& rhs);

    bool operator != (const Species& rhs);

    // friend bool operator == (const char& lhs, const Species& rhs)

    bool is_species(const char& lhs);

    void add_instruction(pair<Species::instruction, int> i);

    pair<Species::instruction, int> next_action(int& pc, const bool& enemy, const bool& empty, const bool& wall);
};

/**
 * Creature Class
 * Stores the Creature's current species, program counter, direction and the number
 * of turns it has taken in the simulation.
 * Executes all operations that involve change of direction and species
*/
class Creature {
    enum direction {
        n,
        e,
        s,
        w
    };

private:
    Species* _s;
    int _pc;
    Creature::direction _d;
    int _turns;

public:
    Creature(Species* s, Creature::direction d);

    friend ostream& operator << (ostream& w, Creature& c);

    friend Creature::direction char_dir(const char& c);

    Species::instruction execute_turn(Creature* next, bool wall);

    /**
     * Return the coordinate of the space in front of the creature,
     * based on it's current direction
    */
    pair<int, int> next(int& curr_row, int& curr_col);

    bool should_move(int& darwin_turns);

    void turn_left();

    void turn_right();

    void infect(Creature* other);
};

/**
 * Darwin_Board class
 * Handles all aspects of the simulation, including the various movements of the creatures
 * Has capability to add either a single creature or a list of creatures
*/
class Darwin_Board {
private:
    vector<Creature> _creatures;
    vector<vector<Creature*>> _grid;
    int _turns;

    bool is_wall(pair<int, int> coord);

public:
    Darwin_Board(int& rows, int& cols);

    void print_header(ostream& w);

    void print_board(ostream& w);

    void add_creature(Creature& c, int& row, int& col);

    void add_creatures(list<Creature>& cl, list<pair<int, int>>& init_loc);

    void next_turn();
};

// Input reading methods

int test_read(istream& r);

Darwin_Board board_read(istream& r);

int num_creatures_read(istream& r);

void creatures_read(istream& r, vector<Species>& s_v, list<Creature>& to_add, list<pair<int, int>>& locations);

vector<Species> species_init();

void darwin_solve(istream& r, ostream& w);