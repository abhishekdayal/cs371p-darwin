var searchData=
[
  ['_5fcreatures',['_creatures',['../classDarwin__Board.html#a9ac9e8bc625bd99d0e52bb7a7bba43ed',1,'Darwin_Board']]],
  ['_5fd',['_d',['../classCreature.html#a5d12406ec85d3793fba4365d213a4f8b',1,'Creature']]],
  ['_5fgrid',['_grid',['../classDarwin__Board.html#afb43602347601737a7958c8b93e6d5da',1,'Darwin_Board']]],
  ['_5fname',['_name',['../classSpecies.html#a6d723ad143d06b4d5b773268f4c83d09',1,'Species']]],
  ['_5fpc',['_pc',['../classCreature.html#a5778942540a7c320e5b582651afbd269',1,'Creature']]],
  ['_5fprogram',['_program',['../classSpecies.html#a6a60038be758fefbcd8be18556211ab0',1,'Species']]],
  ['_5fs',['_s',['../classCreature.html#ac9111b34f66ec841dacc4ff39b734f9d',1,'Creature']]],
  ['_5fturns',['_turns',['../classCreature.html#acb68df72c3910502f5215248824326aa',1,'Creature::_turns()'],['../classDarwin__Board.html#a846929409e4e6ff3cf6ac4685ce331ad',1,'Darwin_Board::_turns()']]]
];
