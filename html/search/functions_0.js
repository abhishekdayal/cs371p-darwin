var searchData=
[
  ['add_5fcreature',['add_creature',['../classDarwin__Board.html#a09823d7c88c9f040be5266615fdb5345',1,'Darwin_Board::add_creature(Creature &amp;c, int &amp;row, int &amp;col)'],['../classDarwin__Board.html#a09823d7c88c9f040be5266615fdb5345',1,'Darwin_Board::add_creature(Creature &amp;c, int &amp;row, int &amp;col)']]],
  ['add_5fcreatures',['add_creatures',['../classDarwin__Board.html#ad7fd7c6d670c69e3024e26e46c07ec48',1,'Darwin_Board::add_creatures(list&lt; Creature &gt; &amp;cl, list&lt; pair&lt; int, int &gt;&gt; &amp;init_loc)'],['../classDarwin__Board.html#ad7fd7c6d670c69e3024e26e46c07ec48',1,'Darwin_Board::add_creatures(list&lt; Creature &gt; &amp;cl, list&lt; pair&lt; int, int &gt;&gt; &amp;init_loc)']]],
  ['add_5finstruction',['add_instruction',['../classSpecies.html#aa97a9c012c7b451a1619359d68f5cf6f',1,'Species::add_instruction(pair&lt; Species::instruction, int &gt; i)'],['../classSpecies.html#aa97a9c012c7b451a1619359d68f5cf6f',1,'Species::add_instruction(pair&lt; Species::instruction, int &gt; i)']]]
];
