var searchData=
[
  ['n',['n',['../classCreature.html#a4c4a1e8703b61c26c357e0b3fd652c1da1707e6703743237500349a8007f3c905',1,'Creature']]],
  ['next',['next',['../classCreature.html#acf4cf8e6485d87e75f73f3fc53873e2b',1,'Creature::next(int &amp;curr_row, int &amp;curr_col)'],['../classCreature.html#acf4cf8e6485d87e75f73f3fc53873e2b',1,'Creature::next(int &amp;curr_row, int &amp;curr_col)']]],
  ['next_5faction',['next_action',['../classSpecies.html#a918acbb1cd11cd31b793b0e03f7e98b9',1,'Species::next_action(int &amp;pc, const bool &amp;enemy, const bool &amp;empty, const bool &amp;wall)'],['../classSpecies.html#a918acbb1cd11cd31b793b0e03f7e98b9',1,'Species::next_action(int &amp;pc, const bool &amp;enemy, const bool &amp;empty, const bool &amp;wall)']]],
  ['next_5fturn',['next_turn',['../classDarwin__Board.html#a7df214117f3bd03e72593baed7390c11',1,'Darwin_Board::next_turn()'],['../classDarwin__Board.html#a7df214117f3bd03e72593baed7390c11',1,'Darwin_Board::next_turn()']]],
  ['num_5fcreatures_5fread',['num_creatures_read',['../Darwin_8c_09_09.html#a7c6bcc891f0c46aa0bd5c2b4b7926c91',1,'num_creatures_read(istream &amp;r):&#160;Darwin.c++'],['../Darwin_8h.html#a7c6bcc891f0c46aa0bd5c2b4b7926c91',1,'num_creatures_read(istream &amp;r):&#160;Darwin.c++']]]
];
