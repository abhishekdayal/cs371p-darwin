// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Darwin.h"

using namespace std;

// -----------
// TestDarwin
// -----------

// ----
// read
// ----

TEST(DarwinFixture, eval) {

    string s("1\n\n17 13\n30\nf 0 9 s\nr 9 8 w\nt 5 6 s\nh 8 9 n\nf 1 8 s\nr 3 9 s\nf 6 1 n\nt 4 3 s\nr 6 7 n\nf 5 8 w\nr 0 0 n\nr 11 11 n\nr 12 12 n\nr 13 3 n\nr 14 4 n\nr 5 5 n\nf 0 0 e\nh 4 4 s\nt 6 4 s\nr 7 4 n\nr 9 4 n\nt 9 10 w\nf 6 9 s\nh 14 4 e\nt 12 12 w\nf 10 9 s\nr 5 11 w\nr 16 0 n\nt 10 5 w\nf 3 7 n\n100 5\n");
    istringstream istr (s);
    ostringstream ostr;
    vector<Species> species = species_init();
    ASSERT_EQ(species.size(), 4);
    const int p = test_read(istr);
    ASSERT_EQ(p, 1);
    Darwin_Board d = board_read(istr);
    list<Creature> to_add;
    list<pair<int, int>> init_locations;
    int creatures = num_creatures_read(istr);
    ASSERT_EQ(creatures, 30);
    while(creatures) {
        creatures_read(istr, species, to_add, init_locations);
        --creatures;
    }
    ASSERT_EQ(to_add.size(), 30);
    ASSERT_EQ(init_locations.size(), 30);
    d.add_creatures(to_add, init_locations);

    string sss("1\n\n17 13\n30\nf 0 9 s\nr 9 8 w\nt 5 6 s\nh 8 9 n\nf 1 8 s\nr 3 9 s\nf 6 1 n\nt 4 3 s\nr 6 7 n\nf 5 8 w\nr 0 0 n\nr 11 11 n\nr 12 12 n\nr 13 3 n\nr 14 4 n\nr 5 5 n\nf 0 0 e\nh 4 4 s\nt 6 4 s\nr 7 4 n\nr 9 4 n\nt 9 10 w\nf 6 9 s\nh 14 4 e\nt 12 12 w\nf 10 9 s\nr 5 11 w\nr 16 0 n\nt 10 5 w\nf 3 7 n\n100 5\n");
    istringstream i_str (sss);
    darwin_solve(i_str, ostr);


    /*
        int tot_moves;
        int freq;
        istr >> tot_moves;
        istr >> freq;
        // move to next line
        string name;
        getline(istr, name);
        d.print_header(ostr);
        for(int i = 0; i <= tot_moves; ++i) {
            if( (i % freq) == 0 ) {
                d.print_board(ostr);
                if (i + freq <= tot_moves) {
                    ostr << "\n";
                }
            }
            d.next_turn();
        }
    */
}
